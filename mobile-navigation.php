<div class="mobile-nav">
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <label for="menu"></label>
      <button type="button" class="navbar-toggle collapsed menu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://ead3.eadiva.com/about">About</a>
      <a class="navbar-brand" href="http://ead3.eadiva.com/blog">Blog</a>
      <a class="navbar-brand" href="http://ead3.eadiva.com/about-ruth/">Me</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="http://ead3.eadiva.com/contact">Contact</a></li>
        <li><a href="http://ead3.eadiva.com/elements/">List of Elements</a></li>
        <li><a href="http://ead3.eadiva.com/elements-alpha/">Alphabetical List of Elements</a></li>
        <li><a href="http://ead3.eadiva.com/elements-alpha/">Sample EAD Files</a></li>
        <li><a href=""></a></li>
      </ul>
      <!-- Search box -->
      <form class="navbar-form" method="get" action="<?php bloginfo('url'); ?>/" role="search">
        <div class="form-group">
          <input type="text" class="form-control" value="<?php the_search_query(); ?>" placeholder="Search" name="s" id="s">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div><!--end mobile nav-->