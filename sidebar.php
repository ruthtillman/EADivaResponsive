<div id="sidebar" class="col-md-4 col-sm-12 col-xs-12">

<?php if ( is_single() || is_home() || is_404() || is_archive() || is_search() || get_post_meta( get_the_ID(), 'redirect', true ) || get_post_meta( get_the_ID(), 'deprecated', true ) || get_post_meta( get_the_ID(), 'original', true) ) { ?>

<?php } elseif ( get_post_meta( get_the_ID(), 'new', true ) ) { ?>

<div class="version-redirect"><p>This element is new in <strong>EAD3</strong>.</p></div>

<?php } else { ?>

<div class="version-redirect"><p>If you are looking for the EAD 2002 version of this element, please visit <a href="http://eadiva.com/2/<?php the_slug(); ?>">the EAD 2002 element page</a>.</p></div>

<?php } ?>

<ul>

  <?php dynamic_sidebar( 'sidebar' ); ?>

</ul>

</div><!--end sidebar-->