<!-- Needs all the closing stuff though -->

<div id="footer" class="col-md-12 col-sm-12 col-xs-12">

<script type="text/javascript"> Cufon.now(); </script>

<div itemscope itemtype="http://data-vocabulary.org/Person" class="copyright clearfix col-md-8 col-sm-12 col-xs-12">

<p>The EADiva Tag Library was created by <span itemprop="title">metadata librarian/archivist</span> <span itemprop="name">Ruth Kitchin Tillman</span> of <a href="http://ruthtillman.com" itemprop="url">ruthtillman.com</a>.</p> 

</div>

<div class="copyright clearfix col-md-8 col-sm-12 col-xs-12">

<p><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">EADiva Tag Library</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://ruthtillman.com/" property="cc:attributionName" rel="cc:attributionURL">Ruth Kitchin Tillman</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>. Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="http://ead3.archivists.org/schema/" rel="dct:source">http://ead3.archivists.org/schema/</a>. Personal note: any material coming from the tag library is public domain. I request attribution so people will be able to more fully explore the resource but would not press any action if material is not attributed.</p>

</div>

<div class="clearfix copyright col-md-4 col-sm-12 col-xs-12">

<p><a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a></p>    

<p>Original design by <strong><a href="http://www.801red.com">801red</a></strong>. Site recoded and redesigned by Ruth Tillman.</p>

</div>

	<?php wp_footer(); ?>

</div><!--end footer-->

</div><!-- main container-->

</body>

</html>