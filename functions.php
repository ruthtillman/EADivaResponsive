<?php

/* SHORTCODES IN WIDGETS */

	add_filter('widget_text', 'do_shortcode');

/* POST THUMBNAILS */

	add_theme_support( 'post-thumbnails' );

	add_image_size('slide', width, height, true);

/* MENUS */

	register_nav_menus(array(

		'main' => 'Main Menu'

	));

/* Create Post Slug */

function get_the_slug( $id=null ){

  if( empty($id) ):

    global $post;

    if( empty($post) )

      return ''; // No global $post var available.

    $id = $post->ID;

  endif;

  $slug = basename( get_permalink($id) );

  return $slug;

}



/* Actually returns slug kinda */


/**
 * Display the page or post slug
 *
 * Uses get_the_slug() and applies 'the_slug' filter.
 */


function the_slug( $id=null ){

  echo apply_filters( 'the_slug', get_the_slug($id) );

}

/* This is how to use slug */

/* <?php the_slug(); ?> */

/* SIDEBARS */

	$sidebars = array(

		'sidebar' => 'Sidebar'

	);

	foreach ($sidebars as $key => $value) {

		register_sidebar(array(

			'id' => $key,

			'name' => $value,

			'before_widget' => '<li id="%1$s" class="clearfix widget %2$s">',

			'after_widget' => '</li>',

			'before_title' => '<h4 class="title">',

			'after_title' => '</h4>',

		));

	}

/* CUSTOM POST TYPES */

/*add_action('init', 'add_project_type');

function add_project_type()

{

  $labels = array(

    'name' => _x('Projects', 'post type general name'),

    'singular_name' => _x('Project', 'post type singular name'),

    'add_new' => _x('Add New', 'Post'),

    'add_new_item' => __('Add New Project'),

    'edit_item' => __('Edit Project'),

    'new_item' => __('New Project'),

    'view_item' => __('View Project'),

    'search_items' => __('Search Projects'),

    'not_found' =>  __('No Projects found'),

    'not_found_in_trash' => __('No Projects found in Trash'),

    'parent_item_colon' => ''

  );

  $args = array(

    'labels' => $labels,

    'public' => true,

    'publicly_queryable' => true,

    'show_ui' => true,

    'query_var' => true,

    'rewrite' => true,

    'capability_type' => 'post',

    'hierarchical' => true,

    'menu_position' => 5,

    'supports' => array('title','editor','thumbnail','post-formats','custom-fields','excerpt','author','comments'),

    'has_archive' => true,

	'taxonomies' => array('category', 'post_tag', 'parent') // this is IMPORTANT

  );

  register_post_type('project',$args);

} */





/* CUSTOM WIDGETS */



	/* Blank Widget

	register_widget('WP_Widget_Blank_Widget');

	class WP_Widget_Blank_Widget extends WP_Widget {

	

		function WP_Widget_Blank_Widget() {

			$widget_ops = array('classname' => 'widget_blank_widget', 'description' => __( "") );

			$this->WP_Widget('blank_widget', __('Blank Widget'), $widget_ops);

		}

	

		function widget( $args, $instance ) {

			global $post;

			extract($args);

			$title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);

			

			echo $before_widget;

			if ($title) { echo $before_title . $title . $after_title; }

			// widget

			echo $after_widget;

		}

	

		function update( $new_instance, $old_instance ) {

			$instance = $old_instance;

			$instance['title'] = strip_tags($new_instance['title']);

			return $instance;

		}

	

		function form( $instance ) {

			$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );

			$title = strip_tags($instance['title']);

			?><p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p><?php

		}

	}

	*/





/* SHORT CODES */



	/* Blank Shortcode

	function sc_blank() {

		$html = '';

		return $html;

	}

	add_shortcode( 'blank', 'sc_blank' );

	*/


/* ADD MORE TO EXCERPT */

function new_excerpt_more($more) {

       global $post;

	return '...&nbsp;<a href="'. get_permalink($post->ID) . '">Read the Rest&nbsp;&raquo;</a>';

}

add_filter('excerpt_more', 'new_excerpt_more');

function custom_excerpt_length( $length ) {

	return 30;

}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

?>