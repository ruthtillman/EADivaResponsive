<?php get_header(); ?>
  <div id="main" class="container shadow clearfix">
  	<div id="content" class="col-md-8 col-sm-12 col-xs-12">
  		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

				<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>

				<p class="byline">Posted by <?php the_author_posts_link(); ?> on <?php the_time('F jS, Y') ?> | <?php the_category(', ') ?></p>

				<div class="entry">

					<?php if (in_category('12')) { ?>

					<p class="categoryead2002">The contents of this post relate to EAD 2002.</p>

					<?php } ?>

					<?php the_content('Read more'); ?>

				</div>

				<p class="postmetadata"><?php if(has_tag()) { the_tags('Tags: ', ', ', '  '); ?>  |<?php }?> <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;', '', ''); ?></p>

			</div>

		<?php endwhile; ?>

			<div class="navigation">

				<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>

				<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>

			</div>

	<?php else : ?>

		<div class="404 single" id="404-post">
			<h1 class="title">Not Found</h1>
			<div class="entry">
				<p>Sorry, the link was broken or you're looking for something that isn't here. Most tag pages have the URL ead3.eadiva.com/tagname. If that doesn't work, why not try searching below to use the site's internal system?</p>
				<?php get_search_form(); ?>
			</div>
		</div>


	<?php endif; ?>

</div><!--end content-->

<?php get_sidebar(); ?>

<?php get_footer(); ?>