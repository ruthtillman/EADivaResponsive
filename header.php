<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes( $doctype ); ?>>

<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>" charset="<?php bloginfo('charset'); ?>" />
	<!-- Responsive meta tags -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<!-- /Responsive meta tags -->
	
	<!-- External items -->
		<link href='http://fonts.googleapis.com/css?family=Rokkitt:400,700' rel='stylesheet' type='text/css' />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,500,600,700,800' rel='stylesheet' type='text/css' />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- /External scripts -->
	
	<!-- Hosted Bootstrap elements -->
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/bootstrap/css/bootstrap.min.css" type="text/css" media="screen" ></script>
		<script src="<?php bloginfo('template_url'); ?>/bootstrap/js/bootstrap.min.js"></script>
	<!-- /Hosted Bootstrap elements -->

	<!-- Specific Wordpress setups -->
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?><!--enables comment reply-->
		<?php wp_head(); ?>
	<!-- /Specific Wordpress setups -->

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<title><?php if ( !is_front_page() && !is_home() ) { wp_title(''); ?> | <?php } ?><?php bloginfo('name'); ?></title>
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" />
</head>

<body <?php body_class(); ?>>

<?php get_template_part( 'mobile-navigation' ); ?>

<div id="header" class="clearfix">

	<a href="<?php bloginfo('url'); ?>" title="EADiva home link"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/ead3-logo.png" alt="EADiva" title="EADiva Header Icon" /></a>

	<h3><?php echo get_bloginfo ( 'description' );  ?></h3>

</div><!--end header-->

<div id="maincontainer" class="container clearfix">

<?php get_template_part( 'navigation' ); ?>